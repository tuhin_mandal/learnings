package com.tuhin.dsjava.datastructure;

/**
 * Node class of a singly linked list
 */
public class Node {

    int data;
    Node next;

    // constructor
    public Node(int data) {
        this.data = data;
    }
}
